#include <chrono>
#include <iostream>
#include "SimpleNums.h"

#define RANDOM 1

std::vector<std::vector<size_t>> generate_matrix(const size_t size, const size_t max_num)
{
    if(RANDOM)
        srand(time(0));
    std::vector<std::vector<size_t>> matrix(size, std::vector<size_t>(size));
    for (size_t i = 0; i < size; i++)
        for (size_t j = 0; j < size; j++)
            matrix[i][j] = rand() % max_num;
    
    return matrix;
}


int main()
{
    //основные параметры
    const size_t max_num = 1000;
    const size_t matrix_size = 5000;
    const size_t threads_num = 8;
    const size_t top_size = 5;

    //генерация матрицы заданного размера из рандомных чисел
    const std::vector<std::vector<size_t>> matrix = generate_matrix(matrix_size, max_num);

    //поиск наиболее встречающихся простых чисел в нескольких потоках
    auto start_time = std::chrono::system_clock::now();
    const auto nums = SimpleNums::find_top_simple_nums(matrix, max_num, top_size, threads_num);
    auto end_time = std::chrono::system_clock::now();

    for (auto& num : nums)
        std::cout << num << " ";
    std::cout << "--> " << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() << "ms\n";
}
