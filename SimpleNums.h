#pragma once
#include <vector>
#include <map>
#include <atomic>


class SimpleNums
{
public:
    static std::vector<size_t> find_top_simple_nums(const std::vector<std::vector<size_t>>& matrix, const size_t max_num, size_t top_size, const size_t threads_num);

private:
    static void process(const std::vector<std::vector<size_t>>& matrix, std::map<size_t, size_t>& counters, std::atomic<size_t>& count);
    static bool is_simple_num(const size_t num);
    static std::map<size_t, size_t> get_counters(const size_t max_num);
    static bool need_find(const size_t num);
};
