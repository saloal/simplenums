#include <thread>
#include "SimpleNums.h"


std::vector<size_t> SimpleNums::find_top_simple_nums(const std::vector<std::vector<size_t>>& matrix, const size_t max_num, size_t top_size, const size_t threads_num)
{
    //Проверка граничных условий
    std::vector<size_t> top_simple_nums;
    if (matrix.empty() || !threads_num || !top_size || (max_num < 2))
        return top_simple_nums;

    std::vector<std::thread> threads(threads_num);

    //Создаем для каждого потока свой счетчик простых чисел для того, чтобы не тратить время на блокировки
    std::vector<std::map<size_t, size_t>> counters(threads_num);
    for (size_t i = 0; i < counters.size(); i++)
        counters[i] = i ? counters[0] : get_counters(max_num);

    //счетчик обработанных строк в матрице
    std::atomic<size_t> count(0);
    for (size_t i = 0; i < threads_num; i++)
        threads[i] = std::thread(process, std::cref(matrix), std::ref(counters[i]), std::ref(count));

    //ждем выполнения потоков
    for (size_t i = 0; i < threads_num; i++)
        if(threads[i].joinable())
            threads[i].join();

    //складываем результаты в нулевой счетчик для дальнейшей обработки
    for (size_t i = 1; i < threads_num; i++)
    {
        auto counters_0_it = counters[0].begin();
        for (const auto& counters_n_it : counters[i])
            (counters_0_it++)->second += counters_n_it.second;
    }

    //После подсчета повторямости простых чисел в матрице, отсортируем по значениям.
    //С точки зрения расхода памяти - не самый оптимальный вариант. С точки зрения сложности - самый выгодный. IMHO
    std::multimap<size_t, size_t> sort_count;
    for (const auto& it : counters[0])
        if (it.second)
            sort_count.insert({ it.second, it.first });

    //Проверка граничных условий, чтобы не вылезти за рамки массива.
    if (sort_count.size() < top_size)
        top_size = sort_count.size();

    //Т.к. массив отсортирован, добавляем в результат значения с конца
    auto it = sort_count.rbegin();
    for (size_t i = 0; i < top_size; i++)
        top_simple_nums.push_back((it++)->second);

    return top_simple_nums;
}


//Т.к. по условиям задачи количество чисел в матрице больше, чем в диапазоне допустимых числел (5000*5000 > 1000),
//то лучше вычислить все допустимые простые числа и считать их повторяемость в матирце.
//В ином случае нужно было бы проверять кажое число из матрицы.
void SimpleNums::process(const std::vector<std::vector<size_t>>& matrix, std::map<size_t, size_t>& counters, std::atomic<size_t>& count)
{
    if (matrix.empty())
        return;

    const size_t matrix_size = matrix[0].size();
    for (size_t i = count++; i < matrix_size; i = count++)
        for (size_t j = 0; j < matrix_size; j++)
            if (need_find(matrix[i][j]) && (counters.find(matrix[i][j]) != counters.end()))
                counters[matrix[i][j]]++;
}


bool SimpleNums::need_find(const size_t num)
{
    if (num > 2 && !(num % 2))
        return false;
    if (num > 3 && !(num % 3))
        return false;
    if (num > 5 && !(num % 5))
        return false;
    if (num > 7 && !(num % 7))
        return false;

    return true;
}


bool SimpleNums::is_simple_num(const size_t num)
{
    if (num < 2)
        return false;

    for (size_t i = 2; i < num; i++)
        if (num % i == 0)
            return false;

    return true;
}


std::map<size_t, size_t> SimpleNums::get_counters(const size_t max_num)
{
    std::map<size_t, size_t> counters;
    for (size_t i = 2; i <= max_num; i++)
        if (is_simple_num(i))
            counters[i] = 0;
    
    return counters;
}
